import 'package:flutter/material.dart';
import 'package:fimber_catcher_app/app_config.dart';
import 'package:fimber_catcher_app/my_home_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppConfig config = AppConfig.of(context);

    return MaterialApp(
      title: config.appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}